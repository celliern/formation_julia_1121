# Formation Julia

## Contenu de la formation (6h)

### 1ère demi-journée (3h) - Introduction

- Base de la syntaxe
- Spécificité Julia (broadcasting, multiple dispatch, performance)

### 2ème demi-journée (3h) - Visualisation en Julia

- Visualisation statique pour la publication
- Graphes interactifs pour l'exploration

## Installation

Il est conseillé d'utiliser VSCode pour suivre la formation, même si un autre éditeur de code est
possible (Atom, ou directement jupyter pour ceux qui le voudrait).

VSCode est gratuit et disponible en téléchargement sur [code.visualstudio.com](https://code.visualstudio.com).

Il faudra également installer le langage Julia lui même (la version 1.6.3), disponible en
téléchargement sur [julialang.org](https://julialang.org/downloads/). N'oubliez pas d'ajouter Julia
au PATH durant l'installation!

Une fois ces deux éléments téléchargés et installés, il faut installer l'extension Julia sur VSCode
pour profiter de son intégration dans l'éditeur.

![tuto_julia_vscode](./static/tuto_julia_vscode.png)

Une fois que c'est fait, VSCode devrait reconnaitre les fichiers julia que vous ouvrez.

![julia est reconnu](./static/2021-11-15-13-44-00.png)

## Ressources supp

### Introduction Julia

- [Official documentation](https://docs.julialang.org/en/v1/)
- Introduction Julia + dev scientifique [ThinkJulia.jl](https://benlauwens.github.io/ThinkJulia.jl/latest/book.html)
- [Julia_for_MATLAB_Users](https://en.wikibooks.org/wiki/Julia_for_MATLAB_Users)

### Visualisation

- [Plots.jl](https://github.com/JuliaPlots/Plots.jl)
- [StatsPlots.jl](https://github.com/JuliaPlots/StatsPlots.jl)
- [Recombinase.jl](https://piever.github.io/Recombinase.jl/latest/)
- [PlotsGallery.jl](https://goropikari.github.io/PlotsGallery.jl/)
- [Makie.jl](https://makie.juliaplots.org/stable/)

### Quelques librairies de reférences

#### Outils généraux

- [Interpolations.jl](https://github.com/JuliaMath/Interpolations.jl)
- [QuadGK.jl](https://juliamath.github.io/QuadGK.jl/latest/)
- [juliadiff](https://juliadiff.org/)
- Symbolic modeling
  - [juliasymbolics](https://juliasymbolics.org/)
  - [ModelingToolkit.jl](https://github.com/SciML/ModelingToolkit.jl)

#### Science des données

- [DataFrames.jl](https://dataframes.juliadata.org/stable/)
- [DataFramesMeta.jl](https://juliadata.github.io/DataFramesMeta.jl/stable/)
- Lecture des données:
  - [CSV.jl](https://csv.juliadata.org/stable/)
  - [JSON3.jl](https://quinnj.github.io/JSON3.jl/stable/)
  - [Parquet.jl](https://github.com/JuliaIO/Parquet.jl)
- ML / deep ML
  - [MLBase](https://mlbasejl.readthedocs.io/en/latest/)
  - [MLJ.jl](https://github.com/alan-turing-institute/MLJ.jl)
  - [Flux.jl](https://fluxml.ai/Flux.jl/stable/)

#### Séries temporelles

- [Temporal.jl](https://dysonance.github.io/Temporal.jl/latest/overview/)
- [Indicators.jl](https://github.com/dysonance/Indicators.jl)

#### ODE / PDE

- [SurveyofPDEPackages](SurveyofPDEPackages)
- [DifferentialEquations.jl](https://diffeq.sciml.ai/stable/)
- [DiffEqOperators.jl](https://github.com/SciML/DiffEqOperators.jl)
- [OffsetArrays.jl](https://github.com/JuliaArrays/OffsetArrays.jl)