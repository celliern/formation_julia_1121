# %%
using Plots

# %%
x = 1:0.01:10
y = exp.(x)

# %%
plot(x, y)

# %%
p = plot(x, y,
         label="var 1",
         color=:red,
         linestyle=:dash,
         xlabel="x",
         ylabel="y",
         title="My figure"
    )

xlabel!(p, "x (unit)")
p

# %%
bar(randn(5), label="random 1", title="My Title")
plot!(randn(10), seriestype=:bar, label="random 2", alpha=.3)

# %%
plot(randn(40))
plot!(randn(40))
plot!(randn(40))
plot!(randn(40))

# %%
p1 = plot()
p2 = plot(dpi=200)
plot!(p1, x, y, yscale=:log)
plot!(p2, x, 2y, yscale=:log);
display(p1)
display(p2)

# %%
savefig(p1, "my_fig.svg")
savefig(p2, "my_fig.png")

# %%
A = randn(500, 6)

# %%
plot(A, seriestype=:scatter)

# %%

# multiple labels should be 1xn, not a simple vector
labels = reshape(["label $i" for i in 1:6], 1, :)
plot(A, seriestype=:scatter, labels=labels)

# %%
# layout plots
plot(A, seriestype=:scatter, layout=(3, 3), title=labels, leg=false)

# %%
ps = plot(layout=(2, 2))

# %%
scatter!(ps[2, 2], rand(100), smooth=true)

# %%
x = 1:0.1:10
y = exp.(x)

# %%
plot(x, y, fontfamily="serif", dpi=200, size=(400, 200), leg=false, markershape=:circle)
scatter(x, y, primary=false, markershape=:circle, markersize=2)

# %%
plot(
    x, y,
    fontfamily="serif",
    markersize=log10.(y),
    markershape=:circle,
    leg=false
)

# %%
plot(
    x, y,
    fontfamily="serif",
    dpi=200,
    size=(400, 200),
    seriestype = [:path; :scatter],
    primary=[true false]
    )


# %%
l = @layout [
    a{0.3w} [grid(3,3)
             b{0.2h}]
]

plot(
    rand(10, 11),
    layout = l, legend = false, seriestype = [:bar; :scatter; :path],
    title = ["($i)" for j in 1:1, i in 1:11], titleloc = :right, titlefont = font(8)
)

# %%
my_func(x) = cos(x) / x
plot(1:0.01:10, my_func)


# %% 2D plots
x = 0:0.05:2π
y = -π:0.05:π
z(x, y) = cos(x) + sin(y)

# %%
# heatmap(y, x, z)
contourf(y, x, z, color=:viridis)

# %%
wireframe(x, y, z)

# %%
surface(x, y, z)


# %%
using Printf

# %%
x = 1:0.01:5
y = exp.(x)

plot(x, y, yscale=:log, xformatter=(x)->@sprintf("%.2f", x))

# %%
x = 0:0.01:2π
function animate_me(t)
    plot(x, cos.(t .+ x), leg=false, title="time: $t")
end

# %%
anim = @animate for t ∈ 0:0.1:30
    animate_me(t)
end
mp4(anim, "anim_fps15.mp4", fps = 30)
