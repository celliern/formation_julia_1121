# %%
# Backends:
# WebGL integré sur jupyter / VSCode
# GL: rapide, besoin d'une fenetre sup
# Cairo : headless
# ] add WGLMakie
using WGLMakie

# %%
x = range(0, 10, length=100)
y = sin.(x)
lines(x, y)

# %%
x = range(0, 10, length=100)
y = sin.(x)
scatter(x, y)

# %%
x = range(0, 10, length=100)
y1 = sin.(x)
y2 = cos.(x)

lines(x, y1)
lines!(x, y2)
current_figure()

# %%
x = range(0, 10, length=100)
y1 = sin.(x)
y2 = cos.(x)

lines(x, y1, color = :red)
lines!(x, y2, color = :blue)
current_figure()

# %%
x = range(0, 10, length=100)
y1 = sin.(x)
y2 = cos.(x)

scatter(x, y1, color = :red, markersize = range(5, 15, length=100))
sc = scatter!(x, y2, color = range(0, 10, length=100), colormap = :thermal)

current_figure()

# %%
sc.colorrange = (2, 10)

# %%
x = range(0, 10, length=100)
y = sin.(x)

colors = repeat([:crimson, :dodgerblue, :slateblue1, :sienna1, :orchid1], 20)

scatter(x, y, color = colors, markersize = 10)

# %%
x = range(0, 10, length=100)
y1 = sin.(x)
y2 = cos.(x)

lines(x, y1, color = :red, label = "sin")
lines!(x, y2, color = :blue, label = "cos")
axislegend()
current_figure()

# %%
x = LinRange(0, 10, 100)
y = sin.(x)

fig = Figure()
lines(fig[1, 1], x, y, color = :red)
lines(fig[1, 2], x, y, color = :blue)
lines(fig[2, 1:2], x, y, color = :green)

fig

# %%
fig = Figure()
ax1 = Axis(fig[1, 1])
ax2 = Axis(fig[1, 2])
ax3 = Axis(fig[2, 1:2])
fig

# %%
lines!(ax1, 0..10, sin)
lines!(ax2, 0..10, cos)
lines!(ax3, 0..10, sqrt)


# %%
ax1.title = "sin"
ax2.title = "cos"
ax3.title = "sqrt"

ax1.ylabel = "amplitude"
ax3.ylabel = "amplitude"
ax3.xlabel = "time"
fig

# %%
fig = Figure()
ax1, l1 = lines(fig[1, 1], 0..10, sin, color = :red)
ax2, l2 = lines(fig[2, 1], 0..10, cos, color = :blue)
Legend(fig[1:2, 2], [l1, l2], ["sin", "cos"])
fig

# %%
fig, ax, hm = heatmap(randn(20, 20))
Colorbar(fig[1, 2], hm)
fig

# %% ANIMATIONS
using Colors

fig, ax, lineplot = lines(0..10, sin; linewidth=10)

# animation settings
nframes = 30
framerate = 30
hue_iterator = range(0, 360, length=nframes)

# %%
# function my_record(hue)
#     lineplot.color = HSV(hue, 1, 0.75)
# end
# record(my_record, "color_animation.mp4", hue_iterator; framerate = framerate)
# x = 1:0.01:500
# myfunc(x) = 5x

# # %%
# map((x) -> 5x, x)
# map(x) do x
#     return 5x
# end

record(fig, "color_animation.mp4", hue_iterator;
       framerate = framerate) do hue
    lineplot.color = HSV(hue, 1, 0.75)
end

# %%
using Observables

# %%
observable = Observable(0)

# %%
observable[]

obs_func = on(observable) do val
    println("Got an update: ", val)
end

# %%
observable[] = 4

# %%
off(obs_func)

# %%
observable[] = 6

# %%

time = Node(0.0)

# %%
xs = range(0, 7, length=40)

ys_1 = @lift(sin.(xs .- $time))
ys_2 = @lift(cos.(xs .- $time) .+ 3)

fig = lines(xs, ys_1, color = :blue, linewidth = 4,
    axis = (title = @lift("t = $(round($time, digits = 1))"),))
scatter!(xs, ys_2, color = :red, markersize = 15)

# %%
framerate = 30
timestamps = range(0, 2, step=1/framerate)

record(fig, "time_animation.mp4", timestamps;
        framerate = framerate) do t
    time[] = t
end

# %%
points = Node(Point2f[randn(2)])

# %%

fig, ax = scatter(points)
limits!(ax, -4, 4, -4, 4)

fps = 60
nframes = 1200

fig
# %%

for i = 1:nframes
    new_point = Point2f(randn(2))
    points[] = push!(points[], new_point)
    sleep(1/fps) # refreshes the display!
end

# %%
scene = Scene(show_axis = false)

# SSAO attributes are per scene
scene[:SSAO][:radius][] = 5.0
scene[:SSAO][:blur][] = 3
scene[:SSAO][:bias][] = 0.025

box = Rect3(Point3f(-0.5), Vec3f(1))
positions = Node([Point3f(x, y, rand()) for x in -5:5 for y in -5:5])
meshscatter!(scene, positions, marker=box, markersize=1, color=:lightblue, ssao=true)
scene

# %%
for i = 1:nframes
    positions[] = [Point3f(x, y, rand()) for x in -5:5 for y in -5:5]
    sleep(1/fps) # refreshes the display!
end
