using Plots, Unitful
using StatsPlots, UnitfulRecipes

# %%
x = (1:0.01:10) .* u"s"
y = x .^ 2 .* u"J"

# %%
plot(x, y, xunit=u"ms")

# %%
using RDatasets

# %%
using DataFrames
df = DataFrame(a = 1:10, b = 10 .* rand(10), c = 10 .* rand(10))

# %%
@df df plot(:a, [:b :c], colour = [:red :blue])

# %%
plot(df.a, [df.b df.c], colour = [:red :blue])

# %%
@df df scatter(:a, :b, markersize = 4 .* log.(:c .+ 0.1))

# %%
# get 2 -> 3 cols from df
@df df plot(:a, cols(1:3), colour = [:green :red :blue])

# %%
df[!, :red] = rand(10)
# ^() get ride of teh ambiguity : here red could be either the color or the column
@df df plot(:a, [:b :c], colour = ^([:red :blue]))


# %%
using RDatasets
school = RDatasets.dataset("mlmRev","Hsb82")
school

# %%
@df school density(:MAch, group = {Sex = :Sx, :Sector}, legend = :topleft)

# %%
using RDatasets
iris = dataset("datasets","iris")
iris

# %%
@df iris marginalkde(:PetalLength, :PetalWidth; bins=30)

# %%
@df iris corrplot([:SepalLength :SepalWidth :PetalLength :PetalWidth], grid = false)

# %%
@df iris cornerplot([:SepalLength :SepalWidth :PetalLength :PetalWidth])


# %%
import RDatasets
singers = RDatasets.dataset("lattice", "singer")
@df singers violin(string.(:VoicePart), :Height, linewidth=0)
@df singers boxplot!(string.(:VoicePart), :Height, fillalpha=0.75, linewidth=2, primary=false)
@df singers dotplot!(string.(:VoicePart), :Height, marker=(:black, stroke(0)), primary=false)


# %%
@df iris groupedhist(:SepalLength, group = :Species, bar_position = :dodge)

# %%
@df iris groupedhist(:SepalLength, group = :Species, bar_position = :stack)